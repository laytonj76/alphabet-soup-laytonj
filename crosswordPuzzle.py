import sys
import re
import logging
import argparse

class CrosswordPuzzle():
    def __init__(self, inputFile):
        self.cwordInput = inputFile
        self.height = 0
        self.width = 0
        self.cwordPuzzle, self.words = self.__setPuzzle(self.cwordInput)

    def solve(self):
        """
        Initial call to operate on the Crossword Puzzle object to find all words
        """
        for counter in range(0,len(self.words)):
            #if line is not empty
            outputString = self.__searchForWord(self.words[counter].replace(' ',''))
            print(self.words[counter]+" "+outputString)
            
    def __setPuzzle(self, inputFile):
        """
        Parse the input file and set the crossword puzzle and words to search as distinct
        attributes of the object

        :param file inputFile: The crossword file passed in
        :return: crossword puzzle as a matrix, words as a list
        """
        fileToProcess = open(inputFile, 'r')
        lines = fileToProcess.readlines()
        fileToProcess.close()

        matrixSize = re.split(r'\D+',lines[0])

        #set height and weight of the matrix
        self.height = int(matrixSize[0])
        self.width = int(matrixSize[1])
        logging.debug('Working with a '+str(self.height)+' X '+str(self.width)+ ' matrix')

        words = []
        for word in range(self.height+1,len(lines)):
            #if line is not empty
            if(lines[word].strip()  != ""):
                logging.debug('Word: '+lines[word].strip())
                words.append(lines[word].strip())

        cwordPuzzle = []
        for row in range(0,self.height):
            #import pdb; pdb.set_trace()
            cwordPuzzle.append(list(lines[row+1].replace(' ','')))
            logging.debug("Lines row("+str(row)+") + 1: "+ lines[row+1])

        return cwordPuzzle, words

    def __searchForWord(self, wordIn):
        """
        Initial looping to find the first letter match. Once found, pass to 
        function to do iterative checks in all directions

        :param str wordIn: The word being matched as a list to iterate
        :return: output string as <row>:<col> <row>:<col>
                 or 'No match found"
        """
        wordWalk = list(wordIn)
        logging.debug('Word to Find: ' + wordIn)
        for row in range(0,len(self.cwordPuzzle)):
            for column in range(0,len(self.cwordPuzzle[row])):
                if wordWalk[0] == self.cwordPuzzle[row][column]:
                    endCoords = self.__searchForConsecutiveLetters(wordWalk,row,column)
                    if endCoords != 0:
                        outputString=str(row)+":"+str(column)+" "+endCoords
                        logging.debug(outputString)
                        return outputString
            
        return "No match found"

    def __searchForConsecutiveLetters(self,word,startRow,startColumn):
        """
        After finding an initial matching character, continue searching in each 
        possible direction (horizontal - forward and backward, vertical - down
        and up, diagonal in all 4 directions)

        :param list word: The word being matched as a list to iterate
        :param int startRow: The row number to identify the initial match
        :param int startColumn: The column number to identify the initial match
        :return: the ending row:col coordinates or 0 (if no match)
        """
        wordLength = len(word)
        # amount of 'horizontal forward space' (hfs) 
        hfs = startColumn + wordLength
        # amount of 'horizontal backward space' (hbs)
        hbs = (startColumn-1) + wordLength
        # amount of 'vertical downward space' (vds)
        vds = startRow + wordLength 
        # amount of 'vertical upward space' (vus)
        vus = (startRow+1) - wordLength

        ##horizontal forward -- same row, increase column 
        # same row, increase column
        if hfs <= self.width:
            matchedCoords = self.__checkForMatch(word,startRow,0,startColumn,1)
            if matchedCoords != 0:
                logging.debug('Matched horizontal')
                return matchedCoords
        #horizontal backward -- same row, decrease column
        if hbs >= 0:
            matchedCoords = self.__checkForMatch(word,startRow,0,startColumn,-1)
            if matchedCoords != 0:
                logging.debug('Matched horizontal backwards')
                return matchedCoords
        #vertical down - increase row, same column
        if vds <= self.height:
            matchedCoords = self.__checkForMatch(word,startRow,1,startColumn,0)
            if matchedCoords != 0:
                logging.debug('Matched vertical down')
                return matchedCoords
        #vertical up - decreased row, same column
        if vus >= 0:
            matchedCoords = self.__checkForMatch(word,startRow,-1,startColumn,0)
            if matchedCoords != 0:
                logging.debug('Matched vertical up')
                return matchedCoords
        #diagonal, down, right - increase row, increase column        
        if vds <= self.height and hfs <= self.width:
            matchedCoords = self.__checkForMatch(word,startRow,1,startColumn,1)
            if matchedCoords != 0:
                logging.debug('Matched diagonal, down, right')
                return matchedCoords
        #diagonal, up, right - decrease row, increase column
        if vus <= 0 and hfs <= self.width:
            matchedCoords = self.__checkForMatch(word,startRow,-1,startColumn,1)
            if matchedCoords != 0:
                logging.debug('Matched diagonal, up, right')
                return matchedCoords
        #diagonal, down, left - increase row, decrease column
        if vds <= self.height and hbs >= 0:
            matchedCoords = self.__checkForMatch(word,startRow,1,startColumn,-1)
            if matchedCoords != 0:
                logging.debug('Matched diagonal, down, left')
                return matchedCoords
        #diagonal, up, left - decrease row, decrease column
        if vus <= 0 and hbs >= 0:
            matchedCoords = self.__checkForMatch(word,startRow,-1,startColumn,-1)
            if matchedCoords != 0:
                logging.debug('Matched diagonal, up, left')
                return matchedCoords
        
        #if it gets here, there's no match
        logging.debug("No Match")
        return 0

    def __checkForMatch(self, word, startRow, rowStep, startColumn, colStep):
        """
        Continue checking the string for a match in the puzzle based on the
        direction of the initial match (i.e. vertical, horizontal, diagonal)

        :param list word: The word being matched as a list to iterate
        :param int startRow: The row number to continue the match
        :param int rowStep: The step (+1, -1, 0) to indicate horizontal direction
        :param int startColumn: The column numbert to continue the match
        :param int colStep: The step (+1, -1, 0) to indicate vertical direction
        :return: the ending row:col coordinates or 0 (if no match)
        """
        wordLength = len(word)
        rowVal = startRow + rowStep
        colVal = startColumn + colStep
        fullMatch = False
        for iterCount in range(1,wordLength):
            if word[iterCount] == self.cwordPuzzle[rowVal][colVal]:
                # still matching
                if iterCount == wordLength-1:
                    #full match
                    fullMatch = True
                else: 
                    #partial match - thusfar
                    rowVal+=rowStep
                    colVal+=colStep
            else:
                # next letter doesn't match; break loop
                break
        #if gets here, full match
        if fullMatch:
            logging.debug('Full match')
            return str(rowVal)+":"+str(colVal)
        else:
            return 0

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", required=True, type=str, help="crossword filename")
    parser.add_argument("-l", "--logging", default="INFO", type=str, help="logging level")
    args = parser.parse_args()

    #basic, generic logging for testing purposes
    logLevel = 'INFO'
    if args.logging.upper() in ['DEBUG','INFO','WARN','CRITICAL','ERROR','EXCEPTION']:
        logLevel = args.logging.upper()
    
    logging.basicConfig(level=logLevel, format='%(levelname)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.debug(args.input)
    
    try:
        cwordSolver = CrosswordPuzzle(args.input)
        cwordSolver.solve()
    except FileNotFoundError as e:
            logging.error("File was not found, please check location")
    except Exception as e:
            logging.error(traceback.format_exc())
