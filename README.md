# Alphabet Soup Execution

The project is designed to take in two parameters from the commandline. 
1. -i (required): specifies the location of the input file
2. -l (optional, default=INFO): specifies level of logging. NOTE: For this test, there are no logging statements above DEBUG. This was used for testing/debugging purposes. 

# Example Usage
*$ python crosswordPuzzle.py -i <file> -l <log_level>*
```
$ python crosswordPuzzle.py -i ~/workspace/crosswordPuzzle
$ python crosswordPuzzle.py -i ~/workspace/crosswordPuzzle -l DEBUG
```  
